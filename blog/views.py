from django.shortcuts import render, redirect, get_object_or_404
from blog.models import Post, Comments, Category, Feedback
from blog.forms import PostForm, CommentForm, FeedbackForm
from datetime import datetime
from django.db.models import Avg
from django.contrib.auth.decorators import login_required
from users.models import User

def post_list(request):
    posts = Post.objects.all().filter(published=True)
    category = Category.objects.all()
    counter = posts.count()
    return render(request, 'blog/post_list.html', {'items': posts, 'category': category,
                                                   'counter': counter})
@login_required
def post_new(request):
    if request.method == "GET":
        form = PostForm()
        return render(request, 'blog/post_new.html', {'form': form})
    else:
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.created_date = datetime.now()
            post.publish_date = datetime.now()
            post.author = request.user
            post.save()
            return redirect('post_detail', post_pk=post.pk)


def categories(request, category_pk):
    posts = Post.objects.filter(category=category_pk)
    category = Category.objects.all()
    return render(request, 'blog/post_list.html', {'items': posts,
                                                   'category': category})
def recomends(request):
    posts = Post.objects.values('title', 'pk').annotate(Avg('feedbacks__rating')).order_by('-feedbacks__rating__avg')[:5]
    return render(request, 'blog/post_recomends.html', {'spisok': posts})

def post_draft(request):
    posts = Post.objects.all().filter(published=False)
    return render(request, 'blog/post_list.html', {'items': posts})


@login_required
def published_post(request, post_pk):
    post = Post.objects.get(pk=post_pk)
    post.published = True
    post.save()
    return render(request, 'blog/post_detail.html', {'post': post})

def rating(post_pk):
    fb = Feedback.objects.filter(post=post_pk)
    count = sum([i.rating for i in fb]) / fb.count()
    return round(count, 1)

def post_detail(request, post_pk):
    post = Post.objects.get(pk=post_pk)
    comments = Comments.objects.all().filter(post=post_pk)
    counter = comments.count()
    if request.method == 'POST':
        comment_form = CommentForm(request.POST)
        if comment_form.is_valid():
            comment = comment_form.save(commit=False)
            user = request.user
            if user.is_authenticated:
                comment.author = request.user
            else:
                return redirect('/users/login/')
            comment.post = post
            comment.published_date = datetime.now()
            comment.save()
            return redirect('post_detail', post_pk=post.pk)
    else:
        comment_form = CommentForm()
    fb = Feedback.objects.filter(post=post_pk)
    counts = round(((sum([i.rating for i in fb])) / fb.count()), 1)
    return render(request, 'blog/post_detail.html', {'post': post,
                                                     'comments': comments,
                                                     'counter': counter,
                                                     'comment_form': comment_form,
                                                     'counts': counts})


@login_required
def post_edit(request, post_pk):
    post = get_object_or_404(Post, pk=post_pk)
    # post = Post.objects.filter(pk=post_pk)
    if request.method == "GET":
        form = PostForm(instance=post)
        return render(request, 'blog/post_edit.html', {'form': form})
    else:
        form = PostForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save(commit=False)
            post.created_date = datetime.now()
            post.publish_date = datetime.now()
            post.save()
            return redirect('post_detail', post_pk=post.pk)

@login_required
def post_delete(request, post_pk):
    post = get_object_or_404(Post, pk=post_pk).delete()
    return redirect('post_list')

@login_required
def comment_delete(request, post_pk, comment_pk):
    post = Post.objects.get(pk=post_pk)
    comment = get_object_or_404(Comments, pk=comment_pk).delete()
    return redirect('post_detail', post_pk=post.pk)


def feedback(request, post_pk):
    post = Post.objects.get(pk=post_pk)
    fb = Feedback.objects.filter(post=post_pk)
    if request.method == 'POST':
        feedback_form = FeedbackForm(request.POST)
        if feedback_form.is_valid():
            feedback = feedback_form.save(commit=False)
            user = request.user
            if user.is_authenticated:
                feedback.author = request.user
            else:
                return redirect('/users/login/')
            feedback.post = post
            feedback.date = datetime.now()
            feedback.save()
            return redirect('feedback', post_pk=post.pk)
    else:
        feedback_form = FeedbackForm()
    context = {'fb': fb,
               'post': post,
               'feedback_form': feedback_form}
    return render(request, 'blog/feedback.html', context)

@login_required
def my_posts(request):
    post = Post.objects.filter(author=request.user)
    return render(request, 'blog/my_posts.html', {'items': post})