from django import forms
from blog.models import Post, Comments, Feedback
from django.forms import ModelForm, Textarea

REVIEW_CHOICES = [('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5')]

class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = (
            "title",
            "text",
        )

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comments
        fields = ('text',
        )
        widgets = {
            'text': Textarea(attrs={'rows': 4, 'cols': 40}),
        }

class FeedbackForm(forms.ModelForm):
    class Meta:
        model = Feedback
        fields = ('text', 'rating'
        )
        widgets = {
            'text': Textarea(attrs={'rows': 4, 'cols': 40}), 'rating': forms.RadioSelect(choices=REVIEW_CHOICES)
        }