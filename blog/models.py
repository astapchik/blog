from django.db import models
from users.models import User
from django.core.validators import MinValueValidator, MaxValueValidator

class Post(models.Model):
    category = models.ForeignKey('Category', on_delete=models.CASCADE,
                                 blank=True, null=True)
    title = models.CharField(
        max_length=64, verbose_name='Title'
    )
    text = models.TextField(
        verbose_name='Text'
    )
    created_date = models.DateTimeField(
        verbose_name='Created Date', auto_now_add=True
    )
    publish_date = models.DateTimeField(
        verbose_name='Publish Date'
    )
    published = models.BooleanField(default=False)

    author = models.ForeignKey(
        User, on_delete=models.CASCADE,
        blank=True, null=True
    )
    def __str__(self):
        return f'{self.title}'

class Comments(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE, blank=True, null=True)
    text = models.TextField(
        verbose_name='Text')
    publish_date = models.DateTimeField(
        auto_now_add=True,
        verbose_name='Publish Date')
    author = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.text}'

class Category(models.Model):
    text = models.CharField(max_length=64, verbose_name='Text')

    def __str__(self):
        return f'{self.text}'

class Feedback(models.Model):
    post = models.ForeignKey(
        Post, on_delete=models.CASCADE, related_name='feedbacks',
    )
    author = models.ForeignKey(
        User,
        on_delete=models.CASCADE
    )
    text = models.TextField(
        verbose_name='Text'
    )
    date = models.DateTimeField(
        auto_now_add=True
    )
    rating = models.IntegerField(
        verbose_name='Rating',
        default=1
    )

    def __str__(self):
        return f'{self.text}'
